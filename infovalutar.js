var _ = require('underscore');
var soap = require('soap');

var wsdl = 'http://infovalutar.ro/curs.asmx?wsdl';

exports.getRates = function(currencyCodes, callback) {
	var date = new Date();
	date.setDate(date.getDate() - 1);
	// Get closest workday (not weekend).
	switch (date.getDay()) {
		case 0: // Sunday
			date.setDate(date.getDate() - 2);
			break;
			
		case 1: // Monday
			date.setDate(date.getDate() - 3);
			break;

		case 6: // Saturday
			date.setDate(date.getDate() - 1);
			break;
	}

	soap.createClient(wsdl, function(err, client) {
		client.getall({dt: date.toISOString()}, function(err, result) {
			console.log(result);
			var currencies = _.filter(result.getallResult.diffgram.DocumentElement.Currency, function(currency) {
				return currencyCodes.indexOf(currency.IDMoneda) != -1;
			});
			var rates = {};
			_.each(currencies, function(currency) {
				rates[currency.IDMoneda] = currency.Value;
			});
			if (currencyCodes.indexOf("RON") != -1) {
				rates["RON"] = 1;
			}
			callback(rates);
		});
	});
};