var nodemailer = require('nodemailer');

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'practice.osfint@gmail.com',
        pass: 'xuyM2#5U'
    }
});

exports.sendMail = function(options) {
	transporter.sendMail(options, function(error, info) {
	    if (error) {
	        console.log(error);
	    } else {
	        console.log("Message sent: " + info.response);
	    }
	});
};

exports.sendInfoMail = function(user, text) {
	exports.sendMail({
		from: 'PracticeOSF-Int <practice.osfint@gmail.com>', // sender address
	    to: user.email, // receivers
	    subject: 'Registration', // Subject line
	    text: text // plaintext body
	});
};