var _ = require("underscore");
var mongo = require("mongodb");
var infovalutar = require("./infovalutar");
var mailer = require("./mailer");

var mongo_db = "practice";
var mongo_username = process.env.OPENSHIFT_MONGODB_DB_USERNAME || null;
var mongo_password = process.env.OPENSHIFT_MONGODB_DB_PASSWORD || null;
var mongo_host = process.env.OPENSHIFT_MONGODB_DB_HOST || "localhost";
var mongo_port = process.env.OPENSHIFT_MONGODB_DB_PORT || 27017;

function createMongoURL() {
	// mongodb://username:password@host:port/db
	var url = "mongodb://";
	if (mongo_username) {
		url += mongo_username;
		if (mongo_password) {
			url += ":" + mongo_password;
		}
		url += "@";
	}
	url += mongo_host + ":" + mongo_port + "/" + mongo_db;
	return url;
}

// find collection by name and filter it (optional)
function find(name, callback, filter) {
	var mdbClient = mongo.MongoClient;
	var mongo_url = createMongoURL();
	mdbClient.connect(mongo_url, function(err, db) {
		var collection = db.collection(name);
		var result;
		if (filter) {
			result = collection.find(filter);
		} else {
			result = collection.find();
		}
		result.toArray(function(err, items) {
			callback(err, items);
			db.close();
		});
	});
}

// insert an object to a collection using mongo .insert() method
function insert(name, docs, options, callback) {
	var mdbClient = mongo.MongoClient;
	var mongo_url = createMongoURL();
	mdbClient.connect(mongo_url, function(err, db) {
		var collection = db.collection(name);
		collection.insert(docs, options, function(err, records) {
			callback(err, records);
			db.close();
		});
	});
}

// update an object in a collection using mongo .insert() method
function update(name, docs, properties, options, callback) {
	var mdbClient = mongo.MongoClient;
	var mongo_url = createMongoURL();
	mdbClient.connect(mongo_url, function(err, db) {
		var collection = db.collection(name);
		collection.update(docs, properties, options, function(err, records) {
			callback(err, records);
			db.close();
		});
	});
}

// when some page is not found (for example category or product)
function render404(req, res, msg) {
	res.render("404", {
		message: msg || "Page not found"
	});
}

exports.index = function(req, res) {
	res.redirect("/shop");
};

exports.shop = function(req, res) {
	find('categories', function(err, items) {
		res.render("shop", {
			_: _,
			currentUrl: req.originalUrl,
			session: req.session,
			title: "BuyOnline shop",
			topCategories: items
		});
	});
};

// Use depth-first search for getting category by id
function buildCategoryChain(root, id, chain) {
	if (root.id == id) {
		chain.push(root);
	} else if (root.categories) {
		for (var i = 0; i < root.categories.length; i++) {
			var child = root.categories[i];
			buildCategoryChain(child, id, chain);
			if (chain.length > 0) {
				chain.push(root);
				return;
			}
		}
	}
}

function productImages(product, type) {
	var imageGroups = product.image_groups;
	var imageGroup = null;
	for (var i = 0; i < imageGroups.length; i++) {
		var group = imageGroups[i];
		if (group.view_type == type && !group.variation_value) {
			imageGroup = group;
			break;
		}
	}
	// ternary operator
	return imageGroup ? imageGroup.images : null;
}

function productImage(product, type) {
	var images = productImages(product, type);
	// ternary operator
	return images && images.length > 0 ? images[0] : null;
}

exports.category = function(req, res) {
	var id = req.params.id;
	if (id) {
		find("categories", function(err, categories) {
			// artificially root element
			var root = {
				name: 'shop',
				categories: categories
			};
			var chain = [];
			buildCategoryChain(root, id, chain);
			if (chain.length > 0) {
				chain = chain.reverse().slice(1);
				var category = chain[chain.length - 1];
				var parents = chain.slice(0, chain.length - 1);
				if (category.categories && category.categories.length > 0) {
					res.render("category", {
						_: _,
						currentUrl: req.originalUrl,
						session: req.session,
						category: category,
						parents: parents,
						topCategories: categories
					});	
				} else {
					find("products", function(err, products) {
						res.render("products", {
							_: _,
							currentUrl: req.originalUrl,
							session: req.session,
							category: category,
							parents: parents,
							topCategories: categories,
							products: products,
							image: productImage
						});
					}, {primary_category_id: id}); 
				}
			} else {
				render404(req, res, "Product's page not found");
			}
		});
	} else {
		render404(req, res, "Category not found");
	}
};

exports.product = function(req, res) {
	var currencyCodes = ["USD", "EUR", "GBP", "RON"];
	var id = req.params.id;
	if (id) {
		find("products", function(err, products) {
			var product = products[0];
			var description = product.long_description;
			var categoryId = product.primary_category_id;
			var currency = product.currency;
			console.log(currency);
			find("categories", function(err, categories) {
				var root = {
					name: 'shop',
					categories: categories
				};
				var chain = [];
				buildCategoryChain(root, categoryId, chain);
				var parents = chain.reverse().slice(1);
				infovalutar.getRates(currencyCodes, function(rates) {
					res.render("product", {
						_: _,
						currentUrl: req.originalUrl,
						session: req.session,
						parents: parents,
						topCategories: categories,
						product: product,
						productImage: productImage,
						productImages: productImages,
						currencyCodes: currencyCodes,
						currencyRates: rates
					});
				});
			});
		}, {id: id});
	} else {
		render404(req, res, "Category not found");
	}
};

function renderRegistrationForm(req, res, email, password, passwordConfirm, errors) {
	email = email || "";
	password = password || "";
	passwordConfirm = passwordConfirm || "";
	errors = errors || {};
	var redirectTo = req.param("redirectTo");

	find('categories', function(err, items) {
		res.render("registration", {
			_: _,
			currentUrl: req.originalUrl,
			redirectTo: redirectTo,
			session: req.session,
			topCategories: items,
			errors: errors,
			email: email,
			password: password,
			passwordConfirm: passwordConfirm
		});
	});
}

function renderLogInForm(req, res, email, password, error) {
	email = email || "";
	password = password || "";
	error = error || "";
	var redirectTo = req.param("redirectTo");

	find('categories', function(err, items) {
		res.render("login", {
			_: _,
			currentUrl: req.originalUrl,
			redirectTo: redirectTo,
			session: req.session,
			topCategories: items,
			error: error,
			email: email,
			password: password
		});
	});
}

exports.register = function(req, res) {
	renderRegistrationForm(req, res);
};

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

exports.doRegister = function(req, res) {
	var redirectTo = req.param("redirectTo");

	var errors = {};

	var email = req.param("email");
	if (!email || email.length == 0) {
		errors["email"] = "Please fill out this field.";
	} else if (!validateEmail(email)) {
		errors["email"] = "Invalid email.";
	}

	var password = req.param("password");
	if (!password || password.length == 0) {
		errors["password"] = "Please fill out this field.";
	} else if (password.length < 6) {
		errors["password"] = "Not long enough (minimum 6 symbols).";
	}

	var passwordConfirm = req.param("passwordConfirm");
	if (!passwordConfirm || passwordConfirm.length == 0) {
		errors["passwordConfirm"] = "Please fill out this field.";
	} else if (password !== passwordConfirm) {
		errors["passwordConfirm"] = "Whoops, these don't match.";
	}

	// validation failed
	if (Object.keys(errors).length > 0) {
		renderRegistrationForm(req, res, email, password, passwordConfirm, errors);
	} else {	//validation passed
		find("users", function(err, users) {
			// check if user with the email already exists
			if (users.length > 0) {
				errors["email"] = "Sorry, this email already exists.";
				renderRegistrationForm(req, res, email, password, passwordConfirm, errors);
			} else {	// insert user to the db
				insert("users", {email: email, password: password}, {w: 1}, function(err, users) {
					var text = "Thank you for registering in our shop. You password is: " + password;
					mailer.sendInfoMail(users[0], text);
					req.session.user = users[0];
					req.session.cart = [];
					res.redirect(redirectTo || "/shop");
				});
			}
		}, {email: email});
	}
};

exports.login = function(req, res) {
	renderLogInForm(req, res);
}

exports.doLogin = function(req, res) {
	var redirectTo = req.param("redirectTo");
	var email = req.param("email");
	var password = req.param("password");
	
	find("users", function(err, users) {
		if (users.length > 0) {
			if (users[0].password == password) {
				console.log(users[0].password);
				req.session.user = users[0];
				req.session.cart = [];
				res.redirect(redirectTo || "/shop");
			} else {
				var error = "Wrong password.";
				renderLogInForm(req, res, email, password, error);
			}
		} else {
			var error = "Wrong email or password. Try again please."
			renderLogInForm(req, res, email, password, error);
		}
	}, {email: email});
};

function renderPassForm(req, res, email, error, succes) {
	email = email || "";
	error = error || "";
	var redirectTo = req.param("redirectTo");

	find('categories', function(err, items) {
		res.render("recover", {
			_: _,
			currentUrl: req.originalUrl,
			redirectTo: redirectTo,
			session: req.session,
			topCategories: items,
			error: error,
			succes: succes,
			email: email
		});
	});
}

exports.recover = function(req, res) {
	renderPassForm(req, res);
}

exports.newPassword = function(req, res) {
	var redirectTo = req.param("redirectTo");
	var email = req.param("email");
	find("users", function(err, users) {
		if (users && users.length > 0) {
			var newPassword = Math.random().toString(36).slice(-8);
			update("users", {email: email}, {$set: {password: newPassword}}, {w: 1}, function(err, users) {
				var succes = true;
				renderPassForm(req, res, email, error, succes);
			});
			find("users", function(req, res) {
				var text = "Your new password is: " + newPassword;
				mailer.sendInfoMail(users[0], text);
			}, {email: email});
		} else {
			var error = "This email is not registered."
			renderPassForm(req, res, email, error);
		}
	}, {email: email});
}

exports.logout = function(req, res) {
	req.session.user = null;
	req.session.cart = null;
	res.redirect("/shop");
};

exports.buy = function(req, res) {
	var productId = req.param("productId");
	var redirectTo = req.param("redirectTo");
	find("products", function(err, products) {
		if (products.length > 0) {
			req.session.cart.push(products[0]);
		}
		res.redirect(redirectTo || "/shop");
	}, {id: productId});
};

exports.cart = function(req, res) {
	var currencyCodes = ["USD", "EUR", "GBP", "RON"];
	var redirectTo = req.param("redirectTo");
	var stats = {};
	_.each(req.session.cart, function(product) {
		if (stats[product.id]) {
			stats[product.id].quantity += 1;
		} else {
			stats[product.id] = {
				product: product,
				quantity: 1
			};
		}
	});
	var products = [];
	for (var id in stats) {
		products.push(stats[id]);
	}
	find('categories', function(err, items) {
		infovalutar.getRates(currencyCodes, function(rates) {
			res.render("cart", {
				_: _,
				currentUrl: req.originalUrl,
				redirectTo: redirectTo,
				session: req.session,
				products: products,
				image: productImage,
				currencyCodes: currencyCodes,
				currencyRates: rates,
				title: "Cart",
				topCategories: items
			});
		});
	});
};

exports.remove = function(req, res) {
	var products = req.session.cart;
	var productId = req.param("productToRemove");
	var index;
	for (var i = 0; i < products.length; i++) {
		if (products[i].id == productId) {
			index = i;
			break;
		}
	}
	if (index >= 0) {
		products.splice(index, 1);
	}
	res.redirect("/shop/cart");
};

exports.deleteAll = function(req, res) {
	req.session.cart = [];
	res.redirect("/shop/cart");
}