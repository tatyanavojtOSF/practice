$(function() {
  $("select#currency").on("change", function() {
    var currency = $(this);
    $(".price").each(function() {
      var price = $(this);
    	var oldCurr = price.data("currency");
    	var newCurr = currency.val(); // selected option value
    	price.data("currency", newCurr);
    	console.log("oldCurr:", oldCurr, "newCurr:", newCurr);

    	var oldRate = parseFloat($("select#currency option[value='" + oldCurr + "']").data("rate"));
    	var newRate = parseFloat($("select#currency option[value='" + newCurr + "']").data("rate"));
    	console.log("oldRate:", oldRate, "newRate:", newRate);

    	var ratio = oldRate / newRate;
    	var oldPrice = parseFloat(price.data("price"));
    	var newPrice = oldPrice * ratio;
    	console.log("oldPrice:", oldPrice, "newPrice:", newPrice);

    	price.data("price", newPrice);
    	price.text(newPrice.toFixed(2) + " " + newCurr);
    });
  });
});