// Module dependencies.
var express = require("express")
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./routes");
var app     = express();

var host = process.env.OPENSHIFT_NODEJS_IP || "localhost";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

// All environments
app.set("host", host);
app.set("port", port);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());

// App routes
app.get("/", routes.index);
app.get("/shop", routes.shop);
app.get("/category/:id?", routes.category);
app.get("/product/:id?", routes.product);
app.get("/shop/register", routes.register);
app.post("/shop/register", routes.doRegister);
app.get("/shop/login", routes.login);
app.post("/shop/login", routes.doLogin);
app.get("/shop/logout", routes.logout);
app.get("/shop/recover", routes.recover);
app.post("/shop/recover", routes.newPassword);
app.get("/shop/buy", routes.buy);
app.get("/shop/cart", routes.cart);
app.get("/shop/cart/remove", routes.remove);
app.get("/shop/cart/deleteAll", routes.deleteAll);

// Run server
http.createServer(app).listen(app.get("port"), app.get("host"), function() {
	console.log("Express server listening on " + app.get("host") + ":" + app.get("port"));
});
